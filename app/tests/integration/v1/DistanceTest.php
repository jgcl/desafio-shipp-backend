<?php

namespace Tests\integration\v1;

use App\Services\DistanceService;
use Tests\TestCase;

/**
 * Class DistanceTest
 * @package Tests\integration\v1
 */
class DistanceTest extends TestCase
{
    /**
     * testo o calculo das distancias
     */
    public function testDistanceFormule(): void
    {
        $distance = DistanceService::vincentyGreatCircleDistance(20, 20, 20, 20);

        $this->assertEquals(0, $distance, 'ooops, calculo da distancia bateu fofo');
    }
}
