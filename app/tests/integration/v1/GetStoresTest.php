<?php

namespace Tests\integration\v1;

use App\Models\CityModel;
use App\Models\CountyModel;
use App\Models\StateModel;
use App\Models\StoreEstablishmentTypeModel;
use App\Models\StoreModel;
use App\Models\StoreOperationTypeModel;
use Tests\TestCase;

/**
 * Class GetStoresTest
 * @package Tests\integration\v1
 */
class GetStoresTest extends TestCase
{
    /**
     * Testando o middleware de validação: tem que vir um 200 no header
     *
     * @return void
     */
    public function testValidationMiddleware200(): void
    {
        $this->get('/V1/stores?longitude=20&latitude=20')
            ->assertResponseStatus(200);
    }

    /**
     * Testando o middleware de validação: tem que vir um 422 no header
     *
     * @return void
     */
    public function testValidationMiddleware422(): void
    {
        $this->get('/V1/stores')
            ->assertResponseStatus(422);
    }

    /**
     * Testando o middleware de validação: tem que vir um 422 no header
     *
     * @return void
     */
    public function testValidationMiddlewareInvalidLatitude(): void
    {
        $this->get('/V1/stores?longitude=20&latitude=20a')
            ->assertResponseStatus(422);
    }


    /**
     * Tem que vir ao menos 1 registro
     *
     * com header correto X-Total >= 1
     * com estrutura correta do json
     *
     * @return void
     */
    public function testStore(): void
    {
        $stateModel = StateModel::create(['name' => 'ES']);
        $cityModel = CityModel::create(['name' => 'Vila Velha', 'state_id' => $stateModel->id]);
        $countyModel = CountyModel::create(['name' => 'Vila Velha']);
        $storeEstablishmentTypeModel = StoreEstablishmentTypeModel::create(['name' => 'JAC']);
        $storeOperationTypeModel = StoreOperationTypeModel::create(['name' => 'Store']);

        StoreModel::create([
            'establishment_type_id' => $storeEstablishmentTypeModel->id,
            'operation_type_id' => $storeOperationTypeModel->id,
            'license_number' => 102030,
            'entity_name' => 'teste',
            'dba_name' => 'teste',
            'square_footage' => 65,

            'street_number' => 363,
            'street_name' => 'rua bahia',
            'address_line2' => null,
            'address_line3' => null,
            'city_id' => $cityModel->id,
            'county_id' => $countyModel->id,
            'zip_code' => 29101370,

            'position' => '20,20',
        ]);

        $response = $this->get('/V1/stores?longitude=20&latitude=20');

        // testo estrutura do json
        $response->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'license_number',
                    'entity_name',
                    'dba_name',
                    'square_footage',
                    'establishment_type' => [
                        'id',
                        'name'
                    ],
                    'operation_type' => [
                        'id',
                        'name'
                    ],
                    'address' => [
                        'id',
                        'street_number',
                        'street_name',
                        'address_line_2',
                        'address_line_3',
                        'zip_code',
                        'city' => [
                            'id',
                            'name',
                        ],
                        'county' => [
                            'id',
                            'name',
                        ],
                        'state' => [
                            'id',
                            'name',
                        ],
                        'street_name',
                    ],
                    'latitude',
                    'longitude',
                    'distance',
                ]
            ]
        ]);

        // tem que ter o header x-total
        $response->seeHeader('X-Total');

        // pego o guzzlehttp
        $response = $response->response;

        // testo o x-total
        if(empty($response->headers->get('X-Total')) || !is_numeric($response->headers->get('X-Total')) || $response->headers->get('X-Total') < 1)
            $this->expectErrorMessage('header x-total nao bate');

        // texto o distante: o primeiro registro tem que ter distância zero, pois estou dando as mesmas coordenadas do registro criado
        if(json_decode($response->getContent())->data[0]->distance != 0)
            $this->expectErrorMessage('problemas no calculo da distancia ou ordenacao');

    }
}
