<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedSmallInteger('establishment_type_id')->index();
            $table->unsignedSmallInteger('operation_type_id')->index();
            $table->unsignedSmallInteger('license_number');
            $table->string('entity_name', 100);
            $table->string('dba_name', 100);
            $table->unsignedSmallInteger('square_footage');

            $table->string('street_number', 10);
            $table->string('street_name', 100);
            $table->string('address_line2', 100)->nullable();
            $table->string('address_line3', 100)->nullable();
            $table->unsignedSmallInteger('county_id')->index();
            $table->unsignedSmallInteger('city_id')->index();
            $table->unsignedSmallInteger('zip_code')->index();
            $table->float('latitude')->nullable();
            $table->float('longitude')->nullable();

            $table->timestamps();

            $table->foreign('establishment_type_id')->references('id')->on('store_establishment_types');
            $table->foreign('operation_type_id')->references('id')->on('store_operation_types');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
