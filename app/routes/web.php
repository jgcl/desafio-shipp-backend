<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// rota de testes
$router->get('/test', [
    'as' => 'test',
    'uses' => 'TestController@get']);

// rota da api v1 das stores
$router->get('/V1/stores', [
    'as' => 'stores',
    'uses' => 'StoreController@get',
    'middleware' => ['validadeLatLngMiddleware', 'logResponseStoresMiddleware']]);
