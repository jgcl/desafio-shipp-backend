<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreOperationTypeModel
 * @package App\Models
 */
class StoreOperationTypeModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "store_operation_types";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
