<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StateModel
 * @package App\Models
 */
class StateModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "states";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'county_id'
    ];
}
