<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreModel
 * @package App\Models
 */
class StoreModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "stores";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'establishment_type_id',
        'operation_type_id',
        'license_number',
        'entity_name',
        'dba_name',
        'square_footage',

        'street_number',
        'street_name',
        'address_line2',
        'address_line3',
        'city_id',
        'county_id',
        'zip_code',

        'latitude',
        'longitude',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'license_number' => 'integer',
        'square_footage' => 'integer',
        'zip_code' => 'integer',
    ];

    /**
     * establishment type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function establishmentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StoreEstablishmentTypeModel::class);
    }

    /**
     * operation type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operationType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StoreOperationTypeModel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CityModel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CountyModel::class);
    }
}
