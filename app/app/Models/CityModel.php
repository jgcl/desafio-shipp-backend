<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryModel
 * @package App\Models
 */
class CityModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "cities";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'state_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StateModel::class);
    }
}
