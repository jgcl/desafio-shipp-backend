<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryModel
 * @package App\Models
 */
class CountyModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "counties";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
