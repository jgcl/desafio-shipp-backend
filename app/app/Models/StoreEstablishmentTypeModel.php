<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreEstablishmentTypeModel
 * @package App\Models
 */
class StoreEstablishmentTypeModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "store_establishment_types";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
