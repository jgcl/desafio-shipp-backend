<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RequestLogModel
 * @package App\Models
 */
class RequestLogModel extends Model
{
    /**
     * The table
     *
     * @var string
     */
    protected $table = "request_logs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'latitude',
        'longitude',
        'stores_returned',
        'status_code'
    ];

    /**
     * disable timestamps in this model
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * dates
     *
     * @var array
     */
    protected $dates = ['date'];
}
