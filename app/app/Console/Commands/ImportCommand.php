<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ImportService;

/**
 * Class ImportCommand
 * @package App\Console\Commands
 */
class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import csv file to sqlite database';

    /**
     * The import service
     *
     * @var ImportService
     */
    private $importService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportService $importService)
    {
        parent::__construct();
        $this->importService = $importService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        // abro o arquivo
        $content = file_get_contents(storage_path('stores.csv'));

        // explodo em linhas
        $lines = explode("\n", $content);

        $this->importService->importFromCsv($lines);



    }
}
