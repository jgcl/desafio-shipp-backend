<?php
/**
 * @SWG\Swagger(
 *     basePath="/",
 *     schemes={"http"},
 *     host="127.0.0.1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Shipp Test API",
 *         description="Shipp Backend Test API description",
 *         @SWG\Contact(
 *             email="gabriel@joaogabriel.org"
 *         ),
 *     )
 * )
 */
