<?php

namespace App\Repositories;

use App\Models\CityModel;
use App\Models\StateModel;

/**
 * Class CountryModel
 * @package App\Models
 */
class CityRepository
{
    /**
     * @param string $name
     * @return CityModel
     */
    public function firstOrCreate(string $name, StateModel $stateModel): CityModel
    {
        return CityModel::firstOrCreate([
            'name' => trim($name),
            'state_id' => $stateModel->id,
        ]);
    }
}
