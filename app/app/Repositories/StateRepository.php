<?php

namespace App\Repositories;

use App\Models\StateModel;

/**
 * Class StateRepository
 * @package App\Models
 */
class StateRepository
{
    /**
     * @param string $name
     * @return StateModel
     */
    public function firstOrCreate(string $name): StateModel
    {
        return StateModel::firstOrCreate([
            'name' => trim($name),
        ]);
    }
}
