<?php

namespace App\Repositories;

use App\Models\StoreEstablishmentTypeModel;

/**
 * Class StoreEstablishmentTypeRepository
 * @package App\Models
 */
class StoreEstablishmentTypeRepository
{
    /**
     * @param string $name
     * @return StoreEstablishmentTypeModel
     */
    public function firstOrCreate(string $name): StoreEstablishmentTypeModel
    {
        return StoreEstablishmentTypeModel::firstOrCreate([
            'name' => trim($name),
        ]);
    }
}
