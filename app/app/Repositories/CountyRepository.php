<?php

namespace App\Repositories;

use App\Models\CountyModel;

/**
 * Class CountyRepository
 * @package App\Models
 */
class CountyRepository
{
    /**
     * @param string $name
     * @return CountyModel
     */
    public function firstOrCreate(string $name): CountyModel
    {
        return CountyModel::firstOrCreate([
            'name' => trim($name),
        ]);
    }
}
