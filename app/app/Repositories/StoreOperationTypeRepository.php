<?php

namespace App\Repositories;

use App\Models\StoreOperationTypeModel;

/**
 * Class StoreOperationTypeRepository
 * @package App\Models
 */
class StoreOperationTypeRepository
{
    /**
     * @param string $name
     * @return StoreOperationTypeModel
     */
    public function firstOrCreate(string $name): StoreOperationTypeModel
    {
        return StoreOperationTypeModel::firstOrCreate([
            'name' => trim($name),
        ]);
    }
}
