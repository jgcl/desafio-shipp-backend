<?php

namespace App\Repositories;

use App\Models\CityModel;
use App\Models\CountyModel;
use App\Models\StoreModel;
use App\Models\StoreOperationTypeModel;
use App\Models\StoreEstablishmentTypeModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class StoreRepository
 * @package App\Models
 */
class StoreRepository
{
    /**
     * @param StoreOperationTypeModel $storeOperationTypeModel
     * @param StoreEstablishmentTypeModel $storeEstablishmentTypeModel
     * @param string $licenseNumber
     * @param string $entityName
     * @param string $dbaName
     * @param string $squareFootage
     * @param string $streetNumber
     * @param string $streetName
     * @param string $addressLine2
     * @param string $addressLine3
     * @param CityModel $cityModel
     * @param CountyModel $countyModel
     * @param string $zipCode
     * @param string $jsonLocation
     *
     * @return StoreModel
     */
    public function create(
        StoreOperationTypeModel $storeOperationTypeModel,
        StoreEstablishmentTypeModel $storeEstablishmentTypeModel,
        string $licenseNumber,
        string $entityName,
        string $dbaName,
        string $squareFootage,

        string $streetNumber,
        string $streetName,
        string $addressLine2,
        string $addressLine3,
        CityModel$cityModel,
        CountyModel $countyModel,
        string $zipCode,
        string $jsonLocation
    ): StoreModel
    {
        $position = $this->getPosition($jsonLocation);

        return StoreModel::firstOrCreate([
            'operation_type_id' => $storeOperationTypeModel->id,
            'establishment_type_id' => $storeEstablishmentTypeModel->id,
            'license_number' => trim($licenseNumber),
            'entity_name' => trim($entityName),
            'dba_name' => trim($dbaName),
            'square_footage' => trim($squareFootage),

            'street_number' => trim($streetNumber),
            'street_name' => trim($streetName),
            'address_line2' => trim($addressLine2) ?? null,
            'address_line3' => trim($addressLine3) ?? null,
            'city_id' => $cityModel->id,
            'county_id' => $countyModel->id,
            'zip_code' => trim($zipCode),
            'latitude' => (float) $position->latitude,
            'longitude' => (float) $position->longitude,
        ]);
    }

    /**
     * @param string $jsonLocation
     * @return \stdClass
     */
    private function getPosition(string $jsonLocation): \stdClass
    {
        // trato $jsonLocation
        $location = $this->getLocationByInvalidJson($jsonLocation);

        if(!$location instanceof \stdClass)
            $location = new \stdClass();

        if(!isset($location->latitude))
            $location->latitude = null;

        if(!isset($location->longitude))
            $location->longitude = null;

        return $location;
    }

    /**
     * @param string $json
     * @return \stdClass|null
     */
    private function getLocationByInvalidJson(string $json): ? \stdClass
    {
        // o json do csv é inválido, porque tem que usar aspas duplas
        // onde tem " trocar por \"
        // ontem tem ' trocar por "
        $newJson = str_replace('"', '\"', $json);
        $newJson = str_replace("'", '"', $newJson);
        $newJson = str_replace("False", "false", $newJson);

        return json_decode($newJson);
    }


    /**
     * retorna lojas por latitude e longitude
     *
     * distancia linear para facilitar
     *
     * @param float $lat
     * @param float $lng
     * @param float $maxDistance
     * @return Collection
     */
    public function getByGeo(float $lat, float $lng, float $maxDistance=6.5): Collection
    {

        // preciso registrar algumas funções
        // https://www.php.net/manual/pt_BR/pdo.sqlitecreatefunction.php
        $pdo = DB::connection('sqlite')->getPdo();
        $pdo->sqliteCreateFunction('sqrt', 'sqrt', 1);
        $pdo->sqliteCreateFunction('asin', 'asin', 1);
        $pdo->sqliteCreateFunction('sin', 'sin', 1);
        $pdo->sqliteCreateFunction('pow', 'pow', 2);
        $pdo->sqliteCreateFunction('pi', 'pi', 0);
        $pdo->sqliteCreateFunction('cos', 'cos', 1);
        $pdo->sqliteCreateFunction('deg2rad', 'deg2rad', 1);

        return StoreModel::query()
            ->selectRaw('
                    *,
                    sqrt( pow(:lat - stores.latitude, 2) + pow(:lng - stores.longitude, 2) ) * 100 as distance
                ',
                [
                      'lat' => $lat,
                      'lng' => $lng,
                ]
            )
            ->where('stores.latitude', '!=', 0)
            ->where('stores.longitude', '!=', 0)
            ->where('distance', '<=', $maxDistance)
            ->orderBy('distance', 'asc')
            ->limit(30)
            ->get();
    }
}
