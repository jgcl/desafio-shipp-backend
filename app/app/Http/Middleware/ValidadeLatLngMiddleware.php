<?php

namespace App\Http\Middleware;

use App\Services\ValidateLatLngService;
use Closure;

class ValidadeLatLngMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function handle($request, Closure $next)
    {
        // validade lat and lng
        new ValidateLatLngService($request);

        return $next($request);
    }
}
