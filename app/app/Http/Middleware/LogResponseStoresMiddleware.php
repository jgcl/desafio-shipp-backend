<?php

namespace App\Http\Middleware;

use App\Services\LogResponseStoresService;
use Closure;

class LogResponseStoresMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        new LogResponseStoresService(
            $request->input('latitude'),
            $request->input('longitude'),
            $response->getStatusCode(),
            $response->headers->get('X-Total') ?? 0
        );

        return $response;
    }
}
