<?php

namespace App\Http\Controllers;

use App\Http\Resources\StoreResource;
use App\Services\StoreService;
use Illuminate\Http\Request;

/**
 * Class StoreController
 * @package App\Http\Controllers
 */
class StoreController extends Controller
{
    /**
     * Pesquisa lojas com base em geolocalização
     *
     * @SWG\Get(
     *      path="/V1/stores",
     *      operationId="StoreController@get",
     *      tags={"Stores"},
     *      summary="Get list of stores",
     *      description="Returns list of stores",
     *
     *      @SWG\Parameter(name="latitude", description="Latitude", required=true, type="string", in="query", default="42.754424"),
     *      @SWG\Parameter(name="longitude", description="Longitude", required=true, type="string", in="query", default="-73.700539"),
     *
     *      @SWG\Response(response=200, description="Stores list"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=500, description="Internal error"),
     * )
     *
     * @param Request $request
     * @param StoreService $storeService
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request, StoreService $storeService)
    {
        $stores = $storeService->getByGeo($request->input('latitude'), $request->input('longitude'));

        return StoreResource::collection($stores)
            ->response()
            ->header('X-Total', count($stores));
    }
}
