<?php

namespace App\Http\Controllers;

use App\Services\DistanceService;

/**
 * Class TestController
 * @package App\Http\Controllers
 */
class TestController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/test",
     *      operationId="testController@get",
     *      tags={"Test"},
     *      summary="Get list of projects",
     *      description="Returns list of projects",
     *
     *      @SWG\Response(response=200, description="Simple test result"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=500, description="Internal error"),
     * )
     *
     * @return array
     */
    public function get(): array
    {
        // casa
        $latitudeFrom = -20.3450227;
        $longitudeFrom = -40.2877446;

        // do lado de casa
        $latitudeToA = -20.344635;
        $longitudeToA = -40.287217;

        // camila
        $latitudeToB = -20.3515763;
        $longitudeToB = -40.2982089;

        // 6,5km
        $latitudeToC = -20.391494;
        $longitudeToC = -40.322608;

        return [
            'a' => DistanceService::distanceKm($latitudeFrom, $longitudeFrom, $latitudeToA, $longitudeToA),
            'a2' => DistanceService::planDistance($latitudeFrom, $longitudeFrom, $latitudeToA, $longitudeToA),

            'b' => DistanceService::distanceKm($latitudeFrom, $longitudeFrom, $latitudeToB, $longitudeToB),
            'b2' => DistanceService::planDistance($latitudeFrom, $longitudeFrom, $latitudeToB, $longitudeToB),

            'c' => DistanceService::distanceKm($latitudeFrom, $longitudeFrom, $latitudeToC, $longitudeToC),
            'c2' => DistanceService::planDistance($latitudeFrom, $longitudeFrom, $latitudeToC, $longitudeToC),
        ];
    }
}
