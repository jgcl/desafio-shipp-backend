<?php

namespace App\Http\Resources;

use App\Services\DistanceService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StoreResource
 * @package App\Http\Resources
 */
class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'license_number' => $this->license_number,
            'entity_name' => $this->entity_name,
            'dba_name' => $this->dba_name,
            'square_footage' => $this->square_footage,
            'establishment_type' => new StoreEstablishmentTypeResource($this->establishmentType),
            'operation_type' => new StoreOperationTypeResource($this->operationType),
            'address' => new AddressResource($this),
            'latitude' => (float) $this->latitude,
            'longitude' => (float) $this->longitude,
            'distance' => (float) number_format($this->distance, 3, '.', ''),
            //'vincenty_distance' => (float) number_format(DistanceService::vincentyGreatCircleDistance($this->latitude, $this->longitude, $request->input('latitude'), $request->input('longitude')), 3, '.', ''),
            //'plan_distance' => (float) number_format(DistanceService::planDistance($this->latitude, $this->longitude, $request->input('latitude'), $request->input('longitude')), 3, '.', ''),
        ];
    }
}
