<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AddressResource
 * @package App\Http\Resources
 */
class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'street_number' => $this->street_number,
            'street_name' => $this->street_name,
            'address_line_2' => $this->address_line_2,
            'address_line_3' => $this->address_line_3,
            'zip_code' => $this->zip_code,
            'city' => new CityResource($this->city),
            'county' => new CountyResource($this->county),
            'state' => new StateResource($this->city->state),
        ];
    }
}
