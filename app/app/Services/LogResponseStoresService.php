<?php

namespace App\Services;

use App\Models\RequestLogModel;
use Carbon\Carbon;

/**
 * Class ValidateLatLngService
 * @package App\Services
 */
class LogResponseStoresService
{
    /**
     * LogResponseStoresService constructor.
     * @param string|null $latitude
     * @param string|null $longitude
     * @param int $statusCode
     * @param int $storesReturned
     */
    public function __construct(?string $latitude, ?string $longitude, int $statusCode, int $storesReturned)
    {
        RequestLogModel::create([
            'date' => Carbon::now(),
            'latitude' => $latitude,
            'longitude' => $longitude,
            'status_code' => $statusCode,
            'stores_returned' => $storesReturned,
        ]);
    }
}
