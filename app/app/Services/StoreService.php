<?php

namespace App\Services;

use App\Repositories\CityRepository;
use App\Repositories\CountyRepository;
use App\Repositories\StateRepository;
use App\Repositories\StoreOperationTypeRepository;
use App\Repositories\StoreRepository;
use App\Repositories\StoreEstablishmentTypeRepository;
use Illuminate\Support\Collection;

/**
 * Class StoreService
 * @package App\Services
 */
class StoreService
{
    /**
     * @var CityRepository
     */
    private $cityRepository;

    /**
     * @var CountyRepository
     */
    private $countyRepository;

    /**
     * @var StateRepository
     */
    private $stateRepository;

    /**
     * @var StoreRepository
     */
    private $storeRepository;

    /**
     * @var StoreEstablishmentTypeRepository
     */
    private $storeEstablishmentTypeRepository;

    /**
     * @var StoreOperationTypeRepository
     */
    private $storeOperationTypeRepository;

    /**
     * ImportService constructor.
     */
    public function __construct()
    {
        $this->cityRepository = new CityRepository();
        $this->countyRepository = new CountyRepository();
        $this->stateRepository = new StateRepository();
        $this->storeRepository = new StoreRepository();
        $this->storeEstablishmentTypeRepository = new StoreEstablishmentTypeRepository();
        $this->storeOperationTypeRepository = new StoreOperationTypeRepository();
    }


    /**
     * search stores using lat and lng
     *
     * @param string $lat
     * @param string $lng
     * @return Collection
     */
    public function getByGeo(float $lat, float $lng): Collection
    {
        // pego no repository
        return $this->storeRepository->getByGeo($lat, $lng, 6.5);
    }

}
