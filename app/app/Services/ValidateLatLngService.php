<?php

namespace App\Services;

use Laravel\Lumen\Routing\ProvidesConvenienceMethods;
use \Illuminate\Http\Request;

/**
 * Class ValidateLatLngService
 * @package App\Services
 */
class ValidateLatLngService
{
    use ProvidesConvenienceMethods;

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __construct(Request $request)
    {
        $this->validate($request, [
            'latitude' => ['required', function ($attribute, $value, $fail) {
                if (!$this->validateLatitude($value)) $fail($attribute.' is invalid.');
            }],
            'longitude' => ['required', function ($attribute, $value, $fail) {
                if (!$this->validateLongitude($value)) $fail($attribute.' is invalid.');
            }],
        ]);
    }

    /**
     * Validates a given latitude $lat
     *
     * https://gist.github.com/arubacao/b5683b1dab4e4a47ee18fd55d9efbdd1
     *
     * @param float|int|string $lat Latitude
     * @return bool `true` if $lat is valid, `false` if not
     */
    private function validateLatitude($lat): bool
    {
        return preg_match('/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/', $lat);
    }

    /**
     * Validates a given longitude $long
     *
     * https://gist.github.com/arubacao/b5683b1dab4e4a47ee18fd55d9efbdd1
     *
     * @param float|int|string $long Longitude
     * @return bool `true` if $long is valid, `false` if not
     */
    private function validateLongitude($long): bool
    {
        return preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/', $long);
    }
}
