<?php

namespace App\Services;

/**
 * Class DistanceService
 * @package App\Services
 */
class DistanceService
{
    /**
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     * @return float
     */
    public static function distanceKm(float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo): float
    {
        $distanceInMeters = self::vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);

        return number_format($distanceInMeters/1000, 2, '.', '');
    }


    /**
     * Calculates the great-circle distance between two points, with the Vincenty formula.
     *
     * https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
     *
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param int $earthRadius Mean earth radius in [m]
     *
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function vincentyGreatCircleDistance(
        float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo, int $earthRadius = 6371000
    ): float
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return ($angle * $earthRadius) / 1000;
    }


    /**
     * calculo plano, precisão de +/- 92% para distancias até 10km
     *
     * https://pt.wikipedia.org/wiki/Coordenadas_geogr%C3%A1ficas
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     * @return float
     */
    public static function planDistance(float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo): float
    {
        // $distance = raizQuadrada( (latitudeFrom-latitudeTo)^2  + (longitudeFrom-longitudeTo)^2 ) -> em um plano
        // $distance = raizQuadrada(     (latitudeFrom-latitudeTo)^2 * 111.12 + (longitudeFrom-longitudeTo)^2 ) * cos( (latitudeFrom-latitudeTo)^2 * 111.12 )     ) -> em uma esfera

        $distance = sqrt( // raiz quadrada
            pow($latitudeFrom-$latitudeTo, 2)
            +
            pow($longitudeFrom-$longitudeTo, 2)
        );

        return number_format($distance*100, 2, '.', '');
    }
}
