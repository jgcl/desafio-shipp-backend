<?php

namespace App\Services;

use App\Repositories\CityRepository;
use App\Repositories\CountyRepository;
use App\Repositories\StateRepository;
use App\Repositories\StoreEstablishmentTypeRepository;
use App\Repositories\StoreOperationTypeRepository;
use App\Repositories\StoreRepository;

/**
 * Class ImportService
 * @package App\Services
 */
class ImportService
{
    /**
     * @var CityRepository
     */
    private $cityRepository;

    /**
     * @var CountyRepository
     */
    private $countyRepository;

    /**
     * @var StateRepository
     */
    private $stateRepository;

    /**
     * @var StoreRepository
     */
    private $storeRepository;

    /**
     * @var StoreEstablishmentTypeRepository
     */
    private $storeEstablishmentTypeRepository;

    /**
     * @var StoreOperationTypeRepository
     */
    private $storeOperationTypeRepository;

    /**
     * ImportService constructor.
     */
    public function __construct()
    {
        $this->cityRepository = new CityRepository();
        $this->countyRepository = new CountyRepository();
        $this->stateRepository = new StateRepository();
        $this->storeRepository = new StoreRepository();
        $this->storeEstablishmentTypeRepository = new StoreEstablishmentTypeRepository();
        $this->storeOperationTypeRepository = new StoreOperationTypeRepository();
    }

    /**
     * import stores from lines (csv file artisan command)
     *
     * @param array $lines
     */
    public function importFromCsv(array $lines)
    {
        // leio linha a linha
        $lineNumber = 0;
        foreach($lines as $line) {
            // linhas vazias (final do arquivo por exemplo)
            // ou a primeira linha (cabeçalho)
            // não precisam ser processadas
            if(empty($line) || $lineNumber == 0) {
                $lineNumber++;
                continue;
            }

            // quebro em colunas
            list($county, $licenseNumber, $operationType, $storeType, $entityName, $dbaName, $streetNumber, $streetName, $addressLine2, $addressLine3, $city, $state, $zipCode, $squareFootage, $jsonLocation) = str_getcsv($line, ",", '');

            // pesquiso county
            $countyModel = $this->countyRepository->firstOrCreate($county);

            // pesquiso state
            $stateModel = $this->stateRepository->firstOrCreate($state);

            // pesquiso city
            $cityModel = $this->cityRepository->firstOrCreate($city, $stateModel);

            // pesquiso operation type
            $storeEstablishmentTypeModel = $this->storeEstablishmentTypeRepository->firstOrCreate($storeType);

            // pesquiso operation type
            $storeOperationTypeModel = $this->storeOperationTypeRepository->firstOrCreate($operationType);

            // insiro
            $this->storeRepository->create(
                $storeOperationTypeModel,
                $storeEstablishmentTypeModel,
                $licenseNumber,
                $entityName,
                $dbaName,
                $squareFootage,

                $streetNumber,
                $streetName,
                $addressLine2,
                $addressLine3,
                $cityModel,
                $countyModel,
                $zipCode,
                $jsonLocation
            );

            echo sprintf("%s\n", $lineNumber);

            $lineNumber++;
        }
    }
}
