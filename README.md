# Solução Desafio Backend
## Autor
- João Gabriel Casteluber Laass
- 27 9 8802-0195
- gabriel@joaogabriel.org
- https://www.linkedin.com/in/joaogabrielcasteluberlaass/

## Defesa
- Utilizei Docker, PHP 7.3 baseado em Alpine (menor tamanho de imagem), Lumen versão 6.x (ideal para APIs rest).

- Fiz o desenvolvimento em 4 camadas: Controller > Service > Repository > Model. Dessa forma eu garanto que se eu mudar o banco de dados meu esforço de conversão será mínimo.

- Criei 02 middlewares: um para validação e um para log (no banco de dados). **O ideal é que o log fosse para o STDOUT do container, assim fica fácil enviar para um Graylog/Kibana da vida.**

- Adicionei um Swagger para facilitar os testes.

- O comando de importação é o `php artisan import:db`. Pode roda-lo mais de uma vez, dados repetidos não serão importados.

- Calculo de distância: encontrei uma formula para PHP, converti para função do SQLite. Se fosse MySQL era mais fácil pois já existe a função **ST_Distance_Sphere** na versão 5.7;

## Como rodar usando o Docker
Com o Docker instalado, basta clonar esse repositório e executar os comandos abaixo:

- Fazer o build (php 7.3 baseado em alpine, composer mais recente, lumen 6.x)

`docker-compose up -d`

- Rodar as migrações:

`docker exec -it api php artisan migrate`

- Rodar os testes:

`docker exec -it api ./vendor/bin/phpunit`

- Rodar a importação do arquivo csv:

`docker exec -it api php artisan import:db`


## Como rodar sem usar o Docker
Com o PHP 7.2+ instalado, basta clocar esse repositório e executar os comandos abaixo:

- Instale as dependências do composer 

`composer install`

- Rodar as migrações:

`php artisan migrate`

- Rodar os testes:

`./vendor/bin/phpunit`

- Rodar a importação do arquivo csv:

`php artisan import:db`

## Consulta de lojas

- `GET http://localhost/V1/stores?lat=42.754424&lng=-73.700539`

## Swagger
Criei uma documentação Swagger dos 2 endpoints criados (um de teste, um do desafio). Abra em seu browser:
- http://localhost

------------------------------
------------------------------
------------------------------
------------------------------
------------------------------


# Desafio Shipp Backend

### Vitrine de Lojas

O objetivo do desafio é construir uma API para consulta e recuperação de uma lista de lojas, ordenada e filtrada por distância. Você deve utilizar
a base de dados de lojas fake disponibilizada no final da descrição do desafio

1) Crie uma modelagem de banco de dados para o dataset fornecido. Utilize um banco de dados sqlite para o desafio. 
Defina a modelagem que seja a mais adequada para a solução, na sua opinião.

2) Implemente um *Comando*, que importa os dados do .csv para o banco .sqlite, esse comando pode ser feito via terminal. E necessário criar um README.md explicando como rodar esse comando.

3) defina uma rota **GET /V1/stores** onde será possível obter todas as lojas hospedadas no banco de dados. A rota deve receber como 
parâmetro um valor de latitude e um valor de longitude. Caso alguém tente acessar este endpoint sem um desses parâmetros, o
sistema deve retornar uma resposta 4XX (Bad Request).

O endpoint deve retornar, no formato JSON, uma lista de lojas ordernadas pela distância em relação ao
ponto fornecido como parâmetro. Assim, em um cenário hipotético, um cliente que acessa o endpoint passando como argumento as 
coordenadas de sua localização atual, receberia como resposta uma lista de lojas, onde as mais próximas ao seu local estariam 
ordernadas no topo da lista. Além disso, a lista deve ser filtrada de acordo com uma distância máxima de 6.5km, de forma que o 
cliente da API não receba na resposta nenhuma loja cujo a distância seja maior do que isso! A localização de cada loja pode ser
encontrada no .csv fornecido na coluna 15 (location). O corpo da resposta deve conter um campo distance em cada loja. Observe que este 
atributo não deve pertencer à modelagem de banco de dados, uma vez que trata-se de um atributo calculado em tempo real, de acordo com a
localização de cada cliente.

O desafio pode ser implementado em qualquer linguagem de backend, dê preferencia as linguagens (PHP,Nodejs,Python,Java,Ruby).

Faça um Fork deste repositório e abra um Pull Request, com seu nome na descrição, para participar.

---
### Diferenciais
- Criar um middleware para verificar a existência ou não dos parâmetros obrigatórios da API.
- Escrever um teste (ou conjunto de testes) que garanta o funcionamento esperado da API.
- Criar um middleware que realize um log de cada request, registrando o horário, o valor de latitude, 
o valor de longitude, o status code e o número de lojas retornadas. Decida se o log será registrado em banco de dados
ou em arquivo simples de texto.

---

[dataset.csv](https://s3-sa-east-1.amazonaws.com/shippmedia/general/stores.csv)