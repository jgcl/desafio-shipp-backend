# imagem base
FROM php:7.3.10-cli-alpine3.10

# copio os arquivos
COPY ./app/ /app/
COPY ./docker.start.sh /docker.start.sh

# diretorio de trabalho
WORKDIR /app

# porta publica
EXPOSE 80

# crio usuario www, instalo composer e chmod no start.sh
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && composer install

# comando que sustenta o container
CMD ["/docker.start.sh"]