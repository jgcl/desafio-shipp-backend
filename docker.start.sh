#!/bin/sh

# só p/ instalar automaticamente em caso de volume nao compartilhado
[ -d "/app/vendor/" ]
  composer install

# servidor embutido no php
php -S 0.0.0.0:80 -t /app/public/